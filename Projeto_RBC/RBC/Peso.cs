﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_RBC.RBC {
    public class Peso {
        public double pesoAtor;
        public double pesoGenero;
        public double pesoDiretor;
        public double pesoDuracao;
        public double pesoFranquia;
        public double pesoRoteiro;
        public double pesoAno;
        public double pesoPais;
    }
}