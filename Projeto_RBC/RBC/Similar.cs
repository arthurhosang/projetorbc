﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_RBC.RBC {
    public class Similar {
        #region fields
        Filme filmeSimilar;
        double similaridade;
        #endregion

        #region properties
        public Filme FilmeSimilar
        {
            get
            {
                return filmeSimilar;
            }

            set
            {
                filmeSimilar = value;
            }
        }
        public double Similaridade
        {
            get
            {
                return similaridade;
            }

            set
            {
                similaridade = value;
            }
        }
        #endregion
    }
}
