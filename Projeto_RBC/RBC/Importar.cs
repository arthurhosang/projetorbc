﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Projeto_RBC.RBC {
    public class Importar {

        #region Methods
        public List<Filme> importarArquivo() {
            #region Fields
            FileStream file;
            StreamReader stream;
            String linhaAtual;
            Array ls;
            bool cabecalho = true;
            List<Filme> filmes = new List<Filme>();
            XmlDocument xml = new XmlDocument();
            OpenFileDialog windowDialog = new OpenFileDialog();
            windowDialog.Filter = "CSV Files| *.csv";
            #endregion

            if (windowDialog.ShowDialog() == DialogResult.OK) {
                try {
                    file = new FileStream(windowDialog.FileName, FileMode.Open);
                    stream = new StreamReader(file, Encoding.Default);

                    while (!stream.EndOfStream) {
                        linhaAtual = stream.ReadLine();

                        if (cabecalho) {
                            cabecalho = false;
                            continue;
                        } else {
                            Filme filme = new Filme();
                            ls = linhaAtual.Split(';');
                            filme.Id = Convert.ToInt32(ls.GetValue(0).ToString().Trim());
                            filme.Titulo = ls.GetValue(1).ToString().Trim();
                            filme.Ator = ls.GetValue(2).ToString().Trim();
                            filme.Genero = ls.GetValue(3).ToString().Trim();
                            filme.Diretor = ls.GetValue(4).ToString().Trim();
                            filme.Duracao = Convert.ToInt32(ls.GetValue(5).ToString().Trim());
                            filme.Franquia = ls.GetValue(6).ToString().Trim();
                            filme.Roteiro = (ls.GetValue(7).ToString().Trim() == "Sim") ? true : false;
                            filme.Ano = Convert.ToInt32(ls.GetValue(8).ToString().Trim());
                            filme.Pais = ls.GetValue(9).ToString().Trim();
                            filmes.Add(filme);
                        }
                    }
                    file.Close();
                    MessageBox.Show("Arquivo importado com sucesso!", "Importação Realizada", MessageBoxButtons.OK, MessageBoxIcon.Information);
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, "Erro de Leitura", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return filmes;
        }
        #endregion
    }
}
