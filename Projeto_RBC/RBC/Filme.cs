﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto_RBC.RBC {
    public class Filme {
        #region fields
        int id;
        string titulo;
        string ator;
        string genero;
        string diretor;
        int duracao;
        string franquia;
        bool roteiro;
        int ano;
        string pais;
        List<Similar> similares;
        #endregion

        #region properties
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        public string Titulo
        {
            get
            {
                return titulo;
            }

            set
            {
                titulo = value;
            }
        }
        public string Ator
        {
            get
            {
                return ator;
            }

            set
            {
                ator = value;
            }
        }
        public string Genero
        {
            get
            {
                return genero;
            }

            set
            {
                genero = value;
            }
        }
        public string Diretor
        {
            get
            {
                return diretor;
            }

            set
            {
                diretor = value;
            }
        }
        public int Duracao
        {
            get
            {
                return duracao;
            }

            set
            {
                duracao = value;
            }
        }
        public string Franquia
        {
            get
            {
                return franquia;
            }

            set
            {
                franquia = value;
            }
        }
        public bool Roteiro
        {
            get
            {
                return roteiro;
            }

            set
            {
                roteiro = value;
            }
        }
        public int Ano
        {
            get
            {
                return ano;
            }

            set
            {
                ano = value;
            }
        }
        public string Pais
        {
            get
            {
                return pais;
            }

            set
            {
                pais = value;
            }
        }
        public List<Similar> Similares
        {
            get
            {
                return similares;
            }

            set
            {
                similares = value;
            }
        }
        #endregion
    }
}
