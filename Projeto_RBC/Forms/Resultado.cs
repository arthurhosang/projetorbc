﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projeto_RBC.RBC;

namespace Projeto_RBC.Forms {
    public partial class Resultado : Form {
        Filme filme = new Filme();

        public Resultado(Filme filmeF) {
            InitializeComponent();
            filme = filmeF;
            mostraResultado();
        }

        public void mostraResultado() {
            DataTable dt = new DataTable();
            dt.Columns.Add("Titulo", typeof(string));
            dt.Columns.Add("Similaridade", typeof(double));

            foreach (Similar similar in filme.Similares) {
                DataRow row = dt.NewRow();
                row[0] = similar.FilmeSimilar.Titulo;
                row[1] = Math.Round(similar.Similaridade, 2);
                dt.Rows.Add(row);
                gridResult.DataSource = dt;
            }
        }
    }
}
