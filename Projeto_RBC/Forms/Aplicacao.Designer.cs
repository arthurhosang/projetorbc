﻿namespace Projeto_RBC {
    partial class Aplicacao {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btImportar = new System.Windows.Forms.Button();
            this.cbFilme = new System.Windows.Forms.ComboBox();
            this.gridFilme = new System.Windows.Forms.DataGridView();
            this.tcFilme = new System.Windows.Forms.TabControl();
            this.tpImportacao = new System.Windows.Forms.TabPage();
            this.tpForm = new System.Windows.Forms.TabPage();
            this.btOk = new System.Windows.Forms.Button();
            this.lbCampos = new System.Windows.Forms.Label();
            this.numAno = new System.Windows.Forms.NumericUpDown();
            this.numDuracao = new System.Windows.Forms.NumericUpDown();
            this.lbPais = new System.Windows.Forms.Label();
            this.lbAno = new System.Windows.Forms.Label();
            this.lbRoteiro = new System.Windows.Forms.Label();
            this.lbFranquia = new System.Windows.Forms.Label();
            this.lbDuracao = new System.Windows.Forms.Label();
            this.lbDiretor = new System.Windows.Forms.Label();
            this.lbGenero = new System.Windows.Forms.Label();
            this.lbTituloForm = new System.Windows.Forms.Label();
            this.lbAtor = new System.Windows.Forms.Label();
            this.cbPais = new System.Windows.Forms.ComboBox();
            this.cbRoteiro = new System.Windows.Forms.ComboBox();
            this.txtFranquia = new System.Windows.Forms.TextBox();
            this.txtDiretor = new System.Windows.Forms.TextBox();
            this.cbGenero = new System.Windows.Forms.ComboBox();
            this.txtAtor = new System.Windows.Forms.TextBox();
            this.tpPeso = new System.Windows.Forms.TabPage();
            this.lbTituloPeso = new System.Windows.Forms.Label();
            this.lbPesoAtor = new System.Windows.Forms.Label();
            this.lbPesoGenero = new System.Windows.Forms.Label();
            this.lbPesoDiretor = new System.Windows.Forms.Label();
            this.lbPesoDuracao = new System.Windows.Forms.Label();
            this.lbPesoFranquia = new System.Windows.Forms.Label();
            this.lbPesoRoteiro = new System.Windows.Forms.Label();
            this.lbPesoAno = new System.Windows.Forms.Label();
            this.lbPesoPais = new System.Windows.Forms.Label();
            this.numPAtor = new System.Windows.Forms.NumericUpDown();
            this.numPGenero = new System.Windows.Forms.NumericUpDown();
            this.numPDiretor = new System.Windows.Forms.NumericUpDown();
            this.numPDuracao = new System.Windows.Forms.NumericUpDown();
            this.numPFranquia = new System.Windows.Forms.NumericUpDown();
            this.numPRoteiro = new System.Windows.Forms.NumericUpDown();
            this.numPAno = new System.Windows.Forms.NumericUpDown();
            this.numPPais = new System.Windows.Forms.NumericUpDown();
            this.btSalvar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridFilme)).BeginInit();
            this.tcFilme.SuspendLayout();
            this.tpImportacao.SuspendLayout();
            this.tpForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDuracao)).BeginInit();
            this.tpPeso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPAtor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPDiretor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPDuracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPFranquia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPRoteiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPAno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPais)).BeginInit();
            this.SuspendLayout();
            // 
            // btImportar
            // 
            this.btImportar.Location = new System.Drawing.Point(6, 6);
            this.btImportar.Name = "btImportar";
            this.btImportar.Size = new System.Drawing.Size(67, 37);
            this.btImportar.TabIndex = 0;
            this.btImportar.Text = "Importar Arquivo";
            this.btImportar.UseVisualStyleBackColor = true;
            this.btImportar.Click += new System.EventHandler(this.btImportar_Click);
            // 
            // cbFilme
            // 
            this.cbFilme.FormattingEnabled = true;
            this.cbFilme.Location = new System.Drawing.Point(79, 15);
            this.cbFilme.Name = "cbFilme";
            this.cbFilme.Size = new System.Drawing.Size(267, 21);
            this.cbFilme.TabIndex = 1;
            this.cbFilme.SelectedValueChanged += new System.EventHandler(this.cbFilme_SelectedValueChanged);
            // 
            // gridFilme
            // 
            this.gridFilme.BackgroundColor = System.Drawing.SystemColors.Control;
            this.gridFilme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridFilme.GridColor = System.Drawing.SystemColors.Control;
            this.gridFilme.Location = new System.Drawing.Point(6, 49);
            this.gridFilme.Name = "gridFilme";
            this.gridFilme.Size = new System.Drawing.Size(340, 457);
            this.gridFilme.TabIndex = 2;
            // 
            // tcFilme
            // 
            this.tcFilme.Controls.Add(this.tpImportacao);
            this.tcFilme.Controls.Add(this.tpForm);
            this.tcFilme.Controls.Add(this.tpPeso);
            this.tcFilme.Location = new System.Drawing.Point(12, 12);
            this.tcFilme.Name = "tcFilme";
            this.tcFilme.SelectedIndex = 0;
            this.tcFilme.Size = new System.Drawing.Size(360, 538);
            this.tcFilme.TabIndex = 0;
            // 
            // tpImportacao
            // 
            this.tpImportacao.Controls.Add(this.gridFilme);
            this.tpImportacao.Controls.Add(this.btImportar);
            this.tpImportacao.Controls.Add(this.cbFilme);
            this.tpImportacao.Location = new System.Drawing.Point(4, 22);
            this.tpImportacao.Name = "tpImportacao";
            this.tpImportacao.Padding = new System.Windows.Forms.Padding(3);
            this.tpImportacao.Size = new System.Drawing.Size(352, 512);
            this.tpImportacao.TabIndex = 0;
            this.tpImportacao.Text = "Lista";
            this.tpImportacao.UseVisualStyleBackColor = true;
            // 
            // tpForm
            // 
            this.tpForm.Controls.Add(this.btOk);
            this.tpForm.Controls.Add(this.lbCampos);
            this.tpForm.Controls.Add(this.numAno);
            this.tpForm.Controls.Add(this.numDuracao);
            this.tpForm.Controls.Add(this.lbPais);
            this.tpForm.Controls.Add(this.lbAno);
            this.tpForm.Controls.Add(this.lbRoteiro);
            this.tpForm.Controls.Add(this.lbFranquia);
            this.tpForm.Controls.Add(this.lbDuracao);
            this.tpForm.Controls.Add(this.lbDiretor);
            this.tpForm.Controls.Add(this.lbGenero);
            this.tpForm.Controls.Add(this.lbTituloForm);
            this.tpForm.Controls.Add(this.lbAtor);
            this.tpForm.Controls.Add(this.cbPais);
            this.tpForm.Controls.Add(this.cbRoteiro);
            this.tpForm.Controls.Add(this.txtFranquia);
            this.tpForm.Controls.Add(this.txtDiretor);
            this.tpForm.Controls.Add(this.cbGenero);
            this.tpForm.Controls.Add(this.txtAtor);
            this.tpForm.Location = new System.Drawing.Point(4, 22);
            this.tpForm.Name = "tpForm";
            this.tpForm.Padding = new System.Windows.Forms.Padding(3);
            this.tpForm.Size = new System.Drawing.Size(352, 512);
            this.tpForm.TabIndex = 1;
            this.tpForm.Text = "Formulário";
            this.tpForm.UseVisualStyleBackColor = true;
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(262, 475);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 17;
            this.btOk.Text = "Concluído";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // lbCampos
            // 
            this.lbCampos.AutoSize = true;
            this.lbCampos.Location = new System.Drawing.Point(10, 480);
            this.lbCampos.Name = "lbCampos";
            this.lbCampos.Size = new System.Drawing.Size(111, 13);
            this.lbCampos.TabIndex = 16;
            this.lbCampos.Text = "* Campos Obrigatórios";
            // 
            // numAno
            // 
            this.numAno.Location = new System.Drawing.Point(89, 351);
            this.numAno.Name = "numAno";
            this.numAno.Size = new System.Drawing.Size(202, 20);
            this.numAno.TabIndex = 6;
            // 
            // numDuracao
            // 
            this.numDuracao.Location = new System.Drawing.Point(89, 201);
            this.numDuracao.Name = "numDuracao";
            this.numDuracao.Size = new System.Drawing.Size(202, 20);
            this.numDuracao.TabIndex = 3;
            // 
            // lbPais
            // 
            this.lbPais.AutoSize = true;
            this.lbPais.Location = new System.Drawing.Point(54, 403);
            this.lbPais.Name = "lbPais";
            this.lbPais.Size = new System.Drawing.Size(29, 13);
            this.lbPais.TabIndex = 15;
            this.lbPais.Text = "País";
            // 
            // lbAno
            // 
            this.lbAno.AutoSize = true;
            this.lbAno.Location = new System.Drawing.Point(53, 353);
            this.lbAno.Name = "lbAno";
            this.lbAno.Size = new System.Drawing.Size(30, 13);
            this.lbAno.TabIndex = 14;
            this.lbAno.Text = "Ano*";
            // 
            // lbRoteiro
            // 
            this.lbRoteiro.AutoSize = true;
            this.lbRoteiro.Location = new System.Drawing.Point(2, 303);
            this.lbRoteiro.Name = "lbRoteiro";
            this.lbRoteiro.Size = new System.Drawing.Size(81, 13);
            this.lbRoteiro.TabIndex = 13;
            this.lbRoteiro.Text = "Roteiro Próprio*";
            // 
            // lbFranquia
            // 
            this.lbFranquia.AutoSize = true;
            this.lbFranquia.Location = new System.Drawing.Point(35, 253);
            this.lbFranquia.Name = "lbFranquia";
            this.lbFranquia.Size = new System.Drawing.Size(48, 13);
            this.lbFranquia.TabIndex = 12;
            this.lbFranquia.Text = "Franquia";
            // 
            // lbDuracao
            // 
            this.lbDuracao.AutoSize = true;
            this.lbDuracao.Location = new System.Drawing.Point(6, 203);
            this.lbDuracao.Name = "lbDuracao";
            this.lbDuracao.Size = new System.Drawing.Size(77, 13);
            this.lbDuracao.TabIndex = 11;
            this.lbDuracao.Text = "Duração (min)*";
            // 
            // lbDiretor
            // 
            this.lbDiretor.AutoSize = true;
            this.lbDiretor.Location = new System.Drawing.Point(45, 153);
            this.lbDiretor.Name = "lbDiretor";
            this.lbDiretor.Size = new System.Drawing.Size(38, 13);
            this.lbDiretor.TabIndex = 10;
            this.lbDiretor.Text = "Diretor";
            // 
            // lbGenero
            // 
            this.lbGenero.AutoSize = true;
            this.lbGenero.Location = new System.Drawing.Point(37, 103);
            this.lbGenero.Name = "lbGenero";
            this.lbGenero.Size = new System.Drawing.Size(46, 13);
            this.lbGenero.TabIndex = 9;
            this.lbGenero.Text = "Gênero*";
            // 
            // lbTituloForm
            // 
            this.lbTituloForm.AutoSize = true;
            this.lbTituloForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTituloForm.Location = new System.Drawing.Point(3, 3);
            this.lbTituloForm.Name = "lbTituloForm";
            this.lbTituloForm.Size = new System.Drawing.Size(242, 20);
            this.lbTituloForm.TabIndex = 8;
            this.lbTituloForm.Text = "Preencha o formulário abaixo";
            // 
            // lbAtor
            // 
            this.lbAtor.AutoSize = true;
            this.lbAtor.Location = new System.Drawing.Point(14, 53);
            this.lbAtor.Name = "lbAtor";
            this.lbAtor.Size = new System.Drawing.Size(69, 13);
            this.lbAtor.TabIndex = 8;
            this.lbAtor.Text = "Ator Principal";
            // 
            // cbPais
            // 
            this.cbPais.FormattingEnabled = true;
            this.cbPais.Location = new System.Drawing.Point(89, 400);
            this.cbPais.Name = "cbPais";
            this.cbPais.Size = new System.Drawing.Size(202, 21);
            this.cbPais.TabIndex = 7;
            // 
            // cbRoteiro
            // 
            this.cbRoteiro.FormattingEnabled = true;
            this.cbRoteiro.Location = new System.Drawing.Point(89, 300);
            this.cbRoteiro.Name = "cbRoteiro";
            this.cbRoteiro.Size = new System.Drawing.Size(202, 21);
            this.cbRoteiro.TabIndex = 5;
            // 
            // txtFranquia
            // 
            this.txtFranquia.Location = new System.Drawing.Point(89, 250);
            this.txtFranquia.Name = "txtFranquia";
            this.txtFranquia.Size = new System.Drawing.Size(202, 20);
            this.txtFranquia.TabIndex = 4;
            // 
            // txtDiretor
            // 
            this.txtDiretor.Location = new System.Drawing.Point(89, 150);
            this.txtDiretor.Name = "txtDiretor";
            this.txtDiretor.Size = new System.Drawing.Size(202, 20);
            this.txtDiretor.TabIndex = 2;
            // 
            // cbGenero
            // 
            this.cbGenero.FormattingEnabled = true;
            this.cbGenero.Location = new System.Drawing.Point(89, 100);
            this.cbGenero.Name = "cbGenero";
            this.cbGenero.Size = new System.Drawing.Size(202, 21);
            this.cbGenero.TabIndex = 1;
            // 
            // txtAtor
            // 
            this.txtAtor.Location = new System.Drawing.Point(89, 50);
            this.txtAtor.Name = "txtAtor";
            this.txtAtor.Size = new System.Drawing.Size(202, 20);
            this.txtAtor.TabIndex = 0;
            // 
            // tpPeso
            // 
            this.tpPeso.Controls.Add(this.btSalvar);
            this.tpPeso.Controls.Add(this.numPPais);
            this.tpPeso.Controls.Add(this.numPAno);
            this.tpPeso.Controls.Add(this.numPRoteiro);
            this.tpPeso.Controls.Add(this.numPFranquia);
            this.tpPeso.Controls.Add(this.numPDuracao);
            this.tpPeso.Controls.Add(this.numPDiretor);
            this.tpPeso.Controls.Add(this.numPGenero);
            this.tpPeso.Controls.Add(this.numPAtor);
            this.tpPeso.Controls.Add(this.lbPesoPais);
            this.tpPeso.Controls.Add(this.lbPesoAno);
            this.tpPeso.Controls.Add(this.lbPesoRoteiro);
            this.tpPeso.Controls.Add(this.lbPesoFranquia);
            this.tpPeso.Controls.Add(this.lbPesoDuracao);
            this.tpPeso.Controls.Add(this.lbPesoDiretor);
            this.tpPeso.Controls.Add(this.lbPesoGenero);
            this.tpPeso.Controls.Add(this.lbPesoAtor);
            this.tpPeso.Controls.Add(this.lbTituloPeso);
            this.tpPeso.Location = new System.Drawing.Point(4, 22);
            this.tpPeso.Name = "tpPeso";
            this.tpPeso.Size = new System.Drawing.Size(352, 512);
            this.tpPeso.TabIndex = 2;
            this.tpPeso.Text = "Pesos";
            this.tpPeso.UseVisualStyleBackColor = true;
            // 
            // lbTituloPeso
            // 
            this.lbTituloPeso.AutoSize = true;
            this.lbTituloPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTituloPeso.Location = new System.Drawing.Point(3, 0);
            this.lbTituloPeso.Name = "lbTituloPeso";
            this.lbTituloPeso.Size = new System.Drawing.Size(225, 20);
            this.lbTituloPeso.TabIndex = 0;
            this.lbTituloPeso.Text = "Preencher os pesos abaixo";
            this.lbTituloPeso.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbPesoAtor
            // 
            this.lbPesoAtor.AutoSize = true;
            this.lbPesoAtor.Location = new System.Drawing.Point(40, 53);
            this.lbPesoAtor.Name = "lbPesoAtor";
            this.lbPesoAtor.Size = new System.Drawing.Size(53, 13);
            this.lbPesoAtor.TabIndex = 1;
            this.lbPesoAtor.Text = "Peso Ator";
            // 
            // lbPesoGenero
            // 
            this.lbPesoGenero.AutoSize = true;
            this.lbPesoGenero.Location = new System.Drawing.Point(24, 102);
            this.lbPesoGenero.Name = "lbPesoGenero";
            this.lbPesoGenero.Size = new System.Drawing.Size(69, 13);
            this.lbPesoGenero.TabIndex = 2;
            this.lbPesoGenero.Text = "Peso Gênero";
            // 
            // lbPesoDiretor
            // 
            this.lbPesoDiretor.AutoSize = true;
            this.lbPesoDiretor.Location = new System.Drawing.Point(28, 153);
            this.lbPesoDiretor.Name = "lbPesoDiretor";
            this.lbPesoDiretor.Size = new System.Drawing.Size(65, 13);
            this.lbPesoDiretor.TabIndex = 3;
            this.lbPesoDiretor.Text = "Peso Diretor";
            // 
            // lbPesoDuracao
            // 
            this.lbPesoDuracao.AutoSize = true;
            this.lbPesoDuracao.Location = new System.Drawing.Point(18, 203);
            this.lbPesoDuracao.Name = "lbPesoDuracao";
            this.lbPesoDuracao.Size = new System.Drawing.Size(75, 13);
            this.lbPesoDuracao.TabIndex = 4;
            this.lbPesoDuracao.Text = "Peso Duração";
            // 
            // lbPesoFranquia
            // 
            this.lbPesoFranquia.AutoSize = true;
            this.lbPesoFranquia.Location = new System.Drawing.Point(18, 253);
            this.lbPesoFranquia.Name = "lbPesoFranquia";
            this.lbPesoFranquia.Size = new System.Drawing.Size(75, 13);
            this.lbPesoFranquia.TabIndex = 5;
            this.lbPesoFranquia.Text = "Peso Franquia";
            // 
            // lbPesoRoteiro
            // 
            this.lbPesoRoteiro.AutoSize = true;
            this.lbPesoRoteiro.Location = new System.Drawing.Point(25, 303);
            this.lbPesoRoteiro.Name = "lbPesoRoteiro";
            this.lbPesoRoteiro.Size = new System.Drawing.Size(68, 13);
            this.lbPesoRoteiro.TabIndex = 6;
            this.lbPesoRoteiro.Text = "Peso Roteiro";
            // 
            // lbPesoAno
            // 
            this.lbPesoAno.AutoSize = true;
            this.lbPesoAno.Location = new System.Drawing.Point(37, 353);
            this.lbPesoAno.Name = "lbPesoAno";
            this.lbPesoAno.Size = new System.Drawing.Size(53, 13);
            this.lbPesoAno.TabIndex = 7;
            this.lbPesoAno.Text = "Peso Ano";
            // 
            // lbPesoPais
            // 
            this.lbPesoPais.AutoSize = true;
            this.lbPesoPais.Location = new System.Drawing.Point(37, 403);
            this.lbPesoPais.Name = "lbPesoPais";
            this.lbPesoPais.Size = new System.Drawing.Size(56, 13);
            this.lbPesoPais.TabIndex = 8;
            this.lbPesoPais.Text = "Peso País";
            // 
            // numPAtor
            // 
            this.numPAtor.Location = new System.Drawing.Point(108, 51);
            this.numPAtor.Name = "numPAtor";
            this.numPAtor.Size = new System.Drawing.Size(120, 20);
            this.numPAtor.TabIndex = 0;
            this.numPAtor.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numPGenero
            // 
            this.numPGenero.Location = new System.Drawing.Point(108, 100);
            this.numPGenero.Name = "numPGenero";
            this.numPGenero.Size = new System.Drawing.Size(120, 20);
            this.numPGenero.TabIndex = 1;
            this.numPGenero.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numPDiretor
            // 
            this.numPDiretor.Location = new System.Drawing.Point(108, 151);
            this.numPDiretor.Name = "numPDiretor";
            this.numPDiretor.Size = new System.Drawing.Size(120, 20);
            this.numPDiretor.TabIndex = 2;
            this.numPDiretor.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // numPDuracao
            // 
            this.numPDuracao.Location = new System.Drawing.Point(108, 201);
            this.numPDuracao.Name = "numPDuracao";
            this.numPDuracao.Size = new System.Drawing.Size(120, 20);
            this.numPDuracao.TabIndex = 3;
            this.numPDuracao.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numPFranquia
            // 
            this.numPFranquia.Location = new System.Drawing.Point(108, 251);
            this.numPFranquia.Name = "numPFranquia";
            this.numPFranquia.Size = new System.Drawing.Size(120, 20);
            this.numPFranquia.TabIndex = 4;
            this.numPFranquia.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numPRoteiro
            // 
            this.numPRoteiro.Location = new System.Drawing.Point(108, 301);
            this.numPRoteiro.Name = "numPRoteiro";
            this.numPRoteiro.Size = new System.Drawing.Size(120, 20);
            this.numPRoteiro.TabIndex = 5;
            this.numPRoteiro.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numPAno
            // 
            this.numPAno.Location = new System.Drawing.Point(108, 351);
            this.numPAno.Name = "numPAno";
            this.numPAno.Size = new System.Drawing.Size(120, 20);
            this.numPAno.TabIndex = 6;
            this.numPAno.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // numPPais
            // 
            this.numPPais.Location = new System.Drawing.Point(108, 401);
            this.numPPais.Name = "numPPais";
            this.numPPais.Size = new System.Drawing.Size(120, 20);
            this.numPPais.TabIndex = 7;
            this.numPPais.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // btSalvar
            // 
            this.btSalvar.Location = new System.Drawing.Point(260, 475);
            this.btSalvar.Name = "btSalvar";
            this.btSalvar.Size = new System.Drawing.Size(75, 23);
            this.btSalvar.TabIndex = 9;
            this.btSalvar.Text = "Salvar";
            this.btSalvar.UseVisualStyleBackColor = true;
            this.btSalvar.Click += new System.EventHandler(this.btSalvar_Click);
            // 
            // Aplicacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 562);
            this.Controls.Add(this.tcFilme);
            this.Name = "Aplicacao";
            this.Text = "Projeto RBC";
            ((System.ComponentModel.ISupportInitialize)(this.gridFilme)).EndInit();
            this.tcFilme.ResumeLayout(false);
            this.tpImportacao.ResumeLayout(false);
            this.tpForm.ResumeLayout(false);
            this.tpForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDuracao)).EndInit();
            this.tpPeso.ResumeLayout(false);
            this.tpPeso.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPAtor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPGenero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPDiretor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPDuracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPFranquia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPRoteiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPAno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPais)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btImportar;
        private System.Windows.Forms.ComboBox cbFilme;
        private System.Windows.Forms.DataGridView gridFilme;
        private System.Windows.Forms.TabControl tcFilme;
        private System.Windows.Forms.TabPage tpImportacao;
        private System.Windows.Forms.TabPage tpForm;
        private System.Windows.Forms.TabPage tpPeso;
        private System.Windows.Forms.Label lbTituloForm;
        private System.Windows.Forms.Label lbAtor;
        private System.Windows.Forms.ComboBox cbPais;
        private System.Windows.Forms.ComboBox cbRoteiro;
        private System.Windows.Forms.TextBox txtFranquia;
        private System.Windows.Forms.TextBox txtDiretor;
        private System.Windows.Forms.ComboBox cbGenero;
        private System.Windows.Forms.TextBox txtAtor;
        private System.Windows.Forms.Label lbPais;
        private System.Windows.Forms.Label lbAno;
        private System.Windows.Forms.Label lbRoteiro;
        private System.Windows.Forms.Label lbFranquia;
        private System.Windows.Forms.Label lbDuracao;
        private System.Windows.Forms.Label lbDiretor;
        private System.Windows.Forms.Label lbGenero;
        private System.Windows.Forms.NumericUpDown numDuracao;
        private System.Windows.Forms.NumericUpDown numAno;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Label lbCampos;
        private System.Windows.Forms.Label lbTituloPeso;
        private System.Windows.Forms.NumericUpDown numPPais;
        private System.Windows.Forms.NumericUpDown numPAno;
        private System.Windows.Forms.NumericUpDown numPRoteiro;
        private System.Windows.Forms.NumericUpDown numPFranquia;
        private System.Windows.Forms.NumericUpDown numPDuracao;
        private System.Windows.Forms.NumericUpDown numPDiretor;
        private System.Windows.Forms.NumericUpDown numPGenero;
        private System.Windows.Forms.NumericUpDown numPAtor;
        private System.Windows.Forms.Label lbPesoPais;
        private System.Windows.Forms.Label lbPesoAno;
        private System.Windows.Forms.Label lbPesoRoteiro;
        private System.Windows.Forms.Label lbPesoFranquia;
        private System.Windows.Forms.Label lbPesoDuracao;
        private System.Windows.Forms.Label lbPesoDiretor;
        private System.Windows.Forms.Label lbPesoGenero;
        private System.Windows.Forms.Label lbPesoAtor;
        private System.Windows.Forms.Button btSalvar;
    }
}

