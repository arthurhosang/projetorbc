﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projeto_RBC.Forms;
using Projeto_RBC.RBC;

namespace Projeto_RBC {
    public partial class Aplicacao : Form {
        #region Atributos
        Importar i;
        Peso peso = new Peso();
        Similar similar;
        double sim, pesoTotal;
        bool v_init, v_form = false, v_peso = false;
        List<Filme> filmes;
        DataTable dt;
        #endregion

        #region Construtor
        public Aplicacao() {
            InitializeComponent();
            tcFilme.TabPages.Remove(tpForm);
            tcFilme.TabPages.Remove(tpPeso);
            init_Pesos();
        }
        #endregion

        #region Methods
        public void inicializa() {
            i = new Importar();
            sim = 0;
            pesoTotal = 0;
            v_init = false;
            filmes = new List<Filme>();
        }
        public void init_Pesos() {
            peso.pesoAno = (double)numPAno.Value;
            peso.pesoAtor = (double)numPAtor.Value;
            peso.pesoDiretor = (double)numPDiretor.Value;
            peso.pesoDuracao = (double)numPDuracao.Value;
            peso.pesoFranquia = (double)numPFranquia.Value;
            peso.pesoGenero = (double)numPGenero.Value;
            peso.pesoPais = (double)numPPais.Value;
            peso.pesoRoteiro = (double)numPRoteiro.Value;
        }
        public void init_cbFilmes() {
            filmes = i.importarArquivo().OrderBy(c => c.Titulo).ToList();
            cbFilme.DataSource = filmes;
            cbFilme.DisplayMember = "Titulo";
            cbFilme.ValueMember = "Id";
            cbFilme.SelectedItem = null;
            v_init = true;
        }
        public void init_cbForm() {
            if (!v_form) {
                cbGenero.Items.AddRange(new string[]{"Ação",
                                    "Animação",
                                    "Aventura",
                                    "Comédia",
                                    "Crime",
                                    "Documentário",
                                    "Drama",
                                    "Ficção Científica",
                                    "Musical",
                                    "Romance",
                                    "Suspense",
                                    "Terror",
                                    "Trash" });

                numDuracao.Minimum = 30;
                numDuracao.Maximum = 200;
                numDuracao.Value = 120;

                cbRoteiro.Items.AddRange(new string[] { "Sim", "Não" });

                numAno.Minimum = 1895;
                numAno.Maximum = DateTime.Now.Year;
                numAno.Value = DateTime.Now.Year;

                cbPais.Items.AddRange(new string[]{"Afeganistão",
                                        "África do Sul",
                                        "Akrotiri",
                                        "Albânia",
                                        "Alemanha",
                                        "Andorra",
                                        "Angola",
                                        "Anguila",
                                        "Antígua e Barbuda",
                                        "Antilhas Neerlandesas",
                                        "Arábia Saudita",
                                        "Argélia",
                                        "Argentina",
                                        "Arménia",
                                        "Aruba",
                                        "Austrália",
                                        "Áustria",
                                        "Azerbaijão",
                                        "Baamas",
                                        "Bangladeche",
                                        "Barbados",
                                        "Barém",
                                        "Bélgica",
                                        "Belize",
                                        "Benim",
                                        "Bermudas",
                                        "Bielorrússia",
                                        "Birmânia",
                                        "Bolívia",
                                        "Bósnia e Herzegovina",
                                        "Botsuana",
                                        "Brasil",
                                        "Brunei",
                                        "Bulgária",
                                        "Burquina Faso",
                                        "Burúndi",
                                        "Butão",
                                        "Cabo Verde",
                                        "Camarões",
                                        "Camboja",
                                        "Canadá",
                                        "Catar",
                                        "Cazaquistão",
                                        "Chade",
                                        "Chile",
                                        "China",
                                        "Chipre",
                                        "Clipperton Island",
                                        "Colômbia",
                                        "Comores",
                                        "Congo-Brazzaville",
                                        "Congo-Kinshasa",
                                        "Coral Sea Islands",
                                        "Coreia do Norte",
                                        "Coreia do Sul",
                                        "Costa do Marfim",
                                        "Costa Rica",
                                        "Croácia",
                                        "Cuba",
                                        "Dhekelia",
                                        "Dinamarca",
                                        "Domínica",
                                        "Egipto",
                                        "Emiratos Árabes Unidos",
                                        "Equador",
                                        "Eritreia",
                                        "Eslováquia",
                                        "Eslovénia",
                                        "Espanha",
                                        "Estados Unidos",
                                        "Estónia",
                                        "Etiópia",
                                        "Faroé",
                                        "Fiji",
                                        "Filipinas",
                                        "Finlândia",
                                        "França",
                                        "Gabão",
                                        "Gâmbia",
                                        "Gana",
                                        "Gaza Strip",
                                        "Geórgia",
                                        "Geórgia do Sul e Sandwich do Sul",
                                        "Gibraltar",
                                        "Granada",
                                        "Grécia",
                                        "Gronelândia",
                                        "Guame",
                                        "Guatemala",
                                        "Guernsey",
                                        "Guiana",
                                        "Guiné",
                                        "Guiné Equatorial",
                                        "Guiné-Bissau",
                                        "Haiti",
                                        "Honduras",
                                        "Hong Kong",
                                        "Hungria",
                                        "Iémen",
                                        "Ilha Bouvet",
                                        "Ilha do Natal",
                                        "Ilha Norfolk",
                                        "Ilhas Caimão",
                                        "Ilhas Cook",
                                        "Ilhas dos Cocos",
                                        "Ilhas Falkland",
                                        "Ilhas Heard e McDonald",
                                        "Ilhas Marshall",
                                        "Ilhas Salomão",
                                        "Ilhas Turcas e Caicos",
                                        "Ilhas Virgens Americanas",
                                        "Ilhas Virgens Britânicas",
                                        "Índia",
                                        "Indian Ocean",
                                        "Indonésia",
                                        "Irão",
                                        "Iraque",
                                        "Irlanda",
                                        "Islândia",
                                        "Israel",
                                        "Itália",
                                        "Jamaica",
                                        "Jan Mayen",
                                        "Japão",
                                        "Jersey",
                                        "Jibuti",
                                        "Jordânia",
                                        "Kuwait",
                                        "Laos",
                                        "Lesoto",
                                        "Letónia",
                                        "Líbano",
                                        "Libéria",
                                        "Líbia",
                                        "Listenstaine",
                                        "Lituânia",
                                        "Luxemburgo",
                                        "Macau",
                                        "Macedónia",
                                        "Madagáscar",
                                        "Malásia",
                                        "Malávi",
                                        "Maldivas",
                                        "Mali",
                                        "Malta",
                                        "Man, Isle of",
                                        "Marianas do Norte",
                                        "Marrocos",
                                        "Maurícia",
                                        "Mauritânia",
                                        "Mayotte",
                                        "México",
                                        "Micronésia",
                                        "Moçambique",
                                        "Moldávia",
                                        "Mónaco",
                                        "Mongólia",
                                        "Monserrate",
                                        "Montenegro",
                                        "Mundo",
                                        "Namíbia",
                                        "Nauru",
                                        "Navassa Island",
                                        "Nepal",
                                        "Nicarágua",
                                        "Níger",
                                        "Nigéria",
                                        "Niue",
                                        "Noruega",
                                        "Nova Caledónia",
                                        "Nova Zelândia",
                                        "Omã",
                                        "Pacific Ocean",
                                        "Países Baixos",
                                        "Palau",
                                        "Panamá",
                                        "Papua-Nova Guiné",
                                        "Paquistão",
                                        "Paracel Islands",
                                        "Paraguai",
                                        "Peru",
                                        "Pitcairn",
                                        "Polinésia Francesa",
                                        "Polónia",
                                        "Porto Rico",
                                        "Portugal",
                                        "Quénia",
                                        "Quirguizistão",
                                        "Quiribáti",
                                        "Reino Unido",
                                        "República Centro-Africana",
                                        "República Checa",
                                        "República Dominicana",
                                        "Roménia",
                                        "Ruanda",
                                        "Rússia",
                                        "Salvador",
                                        "Samoa",
                                        "Samoa Americana",
                                        "Santa Helena",
                                        "Santa Lúcia",
                                        "São Cristóvão e Neves",
                                        "São Marinho",
                                        "São Pedro e Miquelon",
                                        "São Tomé e Príncipe",
                                        "São Vicente e Granadinas",
                                        "Sara Ocidental",
                                        "Seicheles",
                                        "Senegal",
                                        "Serra Leoa",
                                        "Sérvia",
                                        "Singapura",
                                        "Síria",
                                        "Somália",
                                        "Southern Ocean",
                                        "Spratly Islands",
                                        "Sri Lanca",
                                        "Suazilândia",
                                        "Sudão",
                                        "Suécia",
                                        "Suíça",
                                        "Suriname",
                                        "Svalbard e Jan Mayen",
                                        "Tailândia",
                                        "Taiwan",
                                        "Tajiquistão",
                                        "Tanzânia",
                                        "Território Britânico do Oceano Índico",
                                        "Territórios Austrais Franceses",
                                        "Timor Leste",
                                        "Togo",
                                        "Tokelau",
                                        "Tonga",
                                        "Trindade e Tobago",
                                        "Tunísia",
                                        "Turquemenistão",
                                        "Turquia",
                                        "Tuvalu",
                                        "Ucrânia",
                                        "Uganda",
                                        "União Europeia",
                                        "Uruguai",
                                        "Usbequistão",
                                        "Vanuatu",
                                        "Vaticano",
                                        "Venezuela",
                                        "Vietname",
                                        "Wake Island",
                                        "Wallis e Futuna",
                                        "West Bank",
                                        "Zâmbia",
                                        "Zimbabué"});

                tcFilme.TabPages.Add(tpForm);
                v_form = true;
            }
        }
        public void init_cbPeso() {
            if (!v_peso) {
                tcFilme.TabPages.Add(tpPeso);
                v_peso = true;
            }
        }

        public void calculaSimilaridade() {
            pesoTotal = peso.pesoAno + peso.pesoAtor + peso.pesoDiretor + peso.pesoDuracao + peso.pesoFranquia + peso.pesoGenero + peso.pesoPais + peso.pesoRoteiro;

            foreach (Filme filmeOrg in filmes) {
                filmeOrg.Similares = new List<Similar>();

                foreach (Filme filmeAdj in filmes) {
                    if (filmeOrg.Titulo.Equals(filmeAdj.Titulo))
                        continue;

                    sim = 0;
                    similar = new Similar();

                    sim += calculaSimAtor(filmeOrg, filmeAdj);
                    sim += calculaSimGenero(filmeOrg, filmeAdj);
                    sim += calculaSimDiretor(filmeOrg, filmeAdj);
                    sim += calculaSimDuracao(filmeOrg, filmeAdj);
                    sim += calculaSimFranquia(filmeOrg, filmeAdj);
                    sim += calculaSimRoteiro(filmeOrg, filmeAdj);
                    sim += calculaSimAno(filmeOrg, filmeAdj);
                    sim += calculaSimPais(filmeOrg, filmeAdj);

                    similar.FilmeSimilar = filmeAdj;
                    similar.Similaridade = (sim / pesoTotal) * 100;

                    filmeOrg.Similares.Add(similar);
                }
            }
        }
        public double calculaSimAtor(Filme org, Filme adj) {
            return org.Ator.Equals(adj.Ator) ? 1 * peso.pesoAtor : 0;
        }
        public double calculaSimGenero(Filme org, Filme adj) {
            return org.Genero.Equals(adj.Genero) ? 1 * peso.pesoGenero : 0;
        }
        public double calculaSimDiretor(Filme org, Filme adj) {
            return org.Diretor.Equals(adj.Diretor) ? 1 * peso.pesoDiretor : 0;
        }
        public double calculaSimDuracao(Filme org, Filme adj) {
            return Math.Abs((1 - ((org.Duracao - adj.Duracao) / (200 - 30))) * peso.pesoDuracao);
        }
        public double calculaSimFranquia(Filme org, Filme adj) {
            if (String.IsNullOrEmpty(org.Franquia) || String.IsNullOrEmpty(adj.Franquia))
                return 0;
            else
                return org.Franquia.Equals(adj.Franquia) ? 1 * peso.pesoFranquia : 0;
        }
        public double calculaSimRoteiro(Filme org, Filme adj) {
            return (org.Roteiro == adj.Roteiro) ? 1 * peso.pesoRoteiro : 0;
        }
        public double calculaSimAno(Filme org, Filme adj) {
            int difAno = org.Ano - adj.Ano;
            if (difAno <= 5) {
                return 1 * peso.pesoAno;
            }
            if (difAno > 5 && difAno <= 10) {
                return 0.8 * peso.pesoAno;
            }
            if (difAno > 10 && difAno <= 20) {
                return 0.5 * peso.pesoAno;
            }
            if (difAno > 20 && difAno <= 30) {
                return 0.3 * peso.pesoAno;
            }
            return 0;
        }
        public double calculaSimPais(Filme org, Filme adj) {
            return org.Pais.Equals(adj.Pais) ? 1 * peso.pesoPais : 0;
        }

        public bool validaForm() {
            if (cbGenero.SelectedIndex < 0 || cbRoteiro.SelectedIndex < 0) {
                MessageBox.Show("Favor preencher os campos obrigatórios.", "Campos Obrigatórios", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public void algoritmoForm() {
            Filme filmeF = new Filme();

            filmeF.Ano = Convert.ToInt32(numAno.Value);
            filmeF.Ator = txtAtor.Text;
            filmeF.Diretor = txtDiretor.Text;
            filmeF.Duracao = Convert.ToInt32(numDuracao.Value);
            filmeF.Franquia = txtFranquia.Text;
            filmeF.Genero = cbGenero.SelectedItem.ToString();
            filmeF.Pais = (cbPais.SelectedItem != null) ? cbPais.SelectedItem.ToString() : "";
            filmeF.Roteiro = (cbRoteiro.SelectedItem.ToString() == "Sim") ? true : false;

            simForm(filmeF);

            Resultado pageResult = new Resultado(filmeF);
            pageResult.Show();
        }
        public void simForm(Filme filmeOrg) {
            pesoTotal = peso.pesoAno + peso.pesoAtor + peso.pesoDiretor + peso.pesoDuracao + peso.pesoFranquia + peso.pesoGenero + peso.pesoPais + peso.pesoRoteiro;

            foreach (Filme filmeAdj in filmes) {
                similar = new Similar();
                sim = 0;

                sim += calculaSimAtor(filmeOrg, filmeAdj);
                sim += calculaSimGenero(filmeOrg, filmeAdj);
                sim += calculaSimDiretor(filmeOrg, filmeAdj);
                sim += calculaSimDuracao(filmeOrg, filmeAdj);
                sim += calculaSimFranquia(filmeOrg, filmeAdj);
                sim += calculaSimRoteiro(filmeOrg, filmeAdj);
                sim += calculaSimAno(filmeOrg, filmeAdj);
                sim += calculaSimPais(filmeOrg, filmeAdj);

                similar.FilmeSimilar = filmeAdj;
                similar.Similaridade = (sim / pesoTotal) * 100;

                if (filmeOrg.Similares == null)
                {
                    filmeOrg.Similares = new List<Similar>();
                }

                filmeOrg.Similares.Add(similar);
            }
        }
        #endregion

        #region Listeners
        private void btImportar_Click(object sender, EventArgs e) {
            inicializa();
            init_cbFilmes();
            init_cbForm();
            init_cbPeso();
            calculaSimilaridade();
        }
        private void btOk_Click(object sender, EventArgs e) {
            if (validaForm()) {
                algoritmoForm();
            }
        }
        private void btSalvar_Click(object sender, EventArgs e) {
            init_Pesos();
            calculaSimilaridade();
            MessageBox.Show("Os pesos foram salvos com sucesso!", "Pesos Salvos", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void cbFilme_SelectedValueChanged(object sender, EventArgs e) {
            if (v_init) {
                Filme filme = (Filme)cbFilme.SelectedItem;
                dt = new DataTable();
                dt.Columns.Add("Titulo", typeof(string));
                dt.Columns.Add("Similaridade", typeof(double));
                gridFilme.DataSource = null;

                foreach (Similar similar in filme.Similares) {
                    DataRow row = dt.NewRow();
                    row[0] = similar.FilmeSimilar.Titulo;
                    row[1] = Math.Round(similar.Similaridade, 2);
                    dt.Rows.Add(row);
                }
                gridFilme.DataSource = dt;
            }
            #endregion
        }
    }
}